﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ScriptExtension
{
    public class ButtonsLabelsConfigurator
    {
        public String okButtonLabel { get; set; }
        public String yesButtonLabel { get; set; }
        public String noButtonLabel { get; set; }
        public String cancelButtonLabel { get; set; }

        public ButtonsLabelsConfigurator()
        {
            this.okButtonLabel = "";
            this.yesButtonLabel = "";
            this.noButtonLabel = "";
            this.cancelButtonLabel = "";
        }

        public ButtonsLabelsConfigurator(String okButtonLabel, String yesButtonLabel, String noButtonLabel, String cancelButtonLabel)
        {
            this.okButtonLabel = okButtonLabel;
            this.yesButtonLabel = yesButtonLabel;
            this.noButtonLabel = noButtonLabel;
            this.cancelButtonLabel = cancelButtonLabel;
        }
    }
}
