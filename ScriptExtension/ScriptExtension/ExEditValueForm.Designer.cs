﻿namespace ScriptExtension
{
    partial class ExEditValueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.valueToEditLabel = new System.Windows.Forms.Label();
            this.valueToEditTextBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // valueToEditLabel
            // 
            this.valueToEditLabel.AutoSize = true;
            this.valueToEditLabel.Location = new System.Drawing.Point(12, 33);
            this.valueToEditLabel.Name = "valueToEditLabel";
            this.valueToEditLabel.Size = new System.Drawing.Size(39, 13);
            this.valueToEditLabel.TabIndex = 0;
            this.valueToEditLabel.Text = "Label :";
            // 
            // valueToEditTextBox
            // 
            this.valueToEditTextBox.Location = new System.Drawing.Point(15, 77);
            this.valueToEditTextBox.Multiline = true;
            this.valueToEditTextBox.Name = "valueToEditTextBox";
            this.valueToEditTextBox.Size = new System.Drawing.Size(202, 81);
            this.valueToEditTextBox.TabIndex = 1;
            this.valueToEditTextBox.TextChanged += new System.EventHandler(this.valueToEditTextBox_TextChanged);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(15, 173);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(83, 26);
            this.okButton.TabIndex = 2;
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(136, 173);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(81, 26);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // ExEditValueForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(229, 209);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.valueToEditTextBox);
            this.Controls.Add(this.valueToEditLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExEditValueForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label valueToEditLabel;
        private System.Windows.Forms.TextBox valueToEditTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
    }
}