﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptExtension
{
    public partial class ExMsgBoxForm : Form
    {
        private MessageBoxButtons formMbButtons;

        public ExMsgBoxForm(ButtonsLabelsConfigurator buttonsLabels, String Titre, String Message, MessageBoxButtons MbButtons)
        {
            InitializeComponent();
            this.Text = Titre;
            this.formLabel.Text = Message;
            this.TopMost = true;
            this.formMbButtons = MbButtons;

            if (buttonsLabels == null)
            {
                buttonsLabels = new ButtonsLabelsConfigurator();
            }

            switch (MbButtons)
            {
                case MessageBoxButtons.OK:
                    this.button1.Text = buttonsLabels.okButtonLabel;
                    this.button2.Visible = false;
                    this.button3.Visible = false;
                    break;
                case MessageBoxButtons.OKCancel:
                    this.button1.Text = buttonsLabels.cancelButtonLabel;
                    this.button2.Text = buttonsLabels.okButtonLabel;
                    this.button3.Visible = false;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    this.button1.Text = buttonsLabels.cancelButtonLabel;
                    this.button2.Text = buttonsLabels.noButtonLabel;
                    this.button3.Text = buttonsLabels.yesButtonLabel;
                    break;
                case MessageBoxButtons.YesNo:
                    this.button1.Text = buttonsLabels.noButtonLabel;
                    this.button2.Text = buttonsLabels.yesButtonLabel;
                    this.button3.Visible = false;
                    break;
                default:
                    this.button1.Text = buttonsLabels.okButtonLabel;
                    this.button2.Visible = false;
                    this.button3.Visible = false;
                    this.formMbButtons = MessageBoxButtons.OK;
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (this.formMbButtons)
            {
                case MessageBoxButtons.OK:
                    DialogResult = DialogResult.OK;
                    break;
                case MessageBoxButtons.OKCancel:
                    DialogResult = DialogResult.Cancel;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    DialogResult = DialogResult.Cancel;
                    break;
                case MessageBoxButtons.YesNo:
                    DialogResult = DialogResult.No;
                    break;
                default:
                    DialogResult = DialogResult.OK;
                    break;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            switch (this.formMbButtons)
            {
                case MessageBoxButtons.OKCancel:
                    DialogResult = DialogResult.OK;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    DialogResult = DialogResult.No;
                    break;
                case MessageBoxButtons.YesNo:
                    DialogResult = DialogResult.Yes;
                    break;
                default:
                    DialogResult = DialogResult.None;
                    break;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            switch (this.formMbButtons)
            {
                case MessageBoxButtons.YesNoCancel:
                    DialogResult = DialogResult.Yes;
                    break;
                default:
                    DialogResult = DialogResult.None;
                    break;
            }
        }
    }
}
