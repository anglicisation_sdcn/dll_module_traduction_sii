﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;
using System.Reflection;
using System.Windows.Forms; 

namespace ScriptExtension
{
    [ComVisible(true)]    
    public class Extension
    {
        private object mpSite = null;
        public object PnSite
        {
            get { return mpSite; }
            set { mpSite = value; }
        }

        private int screenNumber = 0;
        private ButtonsLabelsConfigurator m_buttonsLabels;
        private int m_FormCloseButton;
        private string m_Message;
        private string m_Titre;
        private int m_MsgButtons;
        private int m_MsgIcon;
        private int m_MsgTimeOut;
        private int m_MsgDefaultSelectedButton;
        private string m_EnteredValue;

        public void ShowOnScreen(String ViewName)
        {
            Screen[] screens = Screen.AllScreens;
            int screenNumber = 0;

            if ((ViewName == null) || ViewName.Equals("M") || ViewName.Equals("G"))
            {
                // Screen MONO or GAUCHE or null
                screenNumber = 0;
            }
            else if (ViewName.Equals("D"))
            {
                // Screen DROIT
                screenNumber = 1;
            }
            else if (ViewName.Equals("H"))
            {
                // Screen HAUT (HAUT_G)
                screenNumber = 2;
            }

            if (screenNumber >= 0 && screenNumber < screens.Length)
            {
                this.screenNumber = screenNumber;
            }
            else
            {
                this.screenNumber = 0;
            }
        }

        public void InitializeButtons(String OkButtonLabel, String YesButtonLabel, String NoButtonLabel, String CancelButtonLabel)
        {
            if (m_buttonsLabels == null)
            {
                m_buttonsLabels = new ButtonsLabelsConfigurator(OkButtonLabel, YesButtonLabel, NoButtonLabel, CancelButtonLabel);
            }
            else
            {
                m_buttonsLabels.okButtonLabel = OkButtonLabel;
                m_buttonsLabels.yesButtonLabel = YesButtonLabel;
                m_buttonsLabels.noButtonLabel = NoButtonLabel;
                m_buttonsLabels.cancelButtonLabel = CancelButtonLabel;
            }
        }

        public int ExMsgBox(string Titre, string Message, int MsgButtons = 0, int MsgIcon = 0, int MsgTimeOut = 0, int MsgDefaultSelectedButton = 0)
        {
            m_Message = Message;
            m_Titre = Titre;
            m_MsgButtons = MsgButtons;
            m_MsgIcon = MsgIcon;
            m_MsgTimeOut = MsgTimeOut;
            m_MsgDefaultSelectedButton = MsgDefaultSelectedButton;

            Form f = new Form();
            f.Location = Screen.AllScreens[this.screenNumber].WorkingArea.Location;
            f.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            f.WindowState = FormWindowState.Maximized;
            f.TopMost = true;
            f.FormBorderStyle = FormBorderStyle.None;
            f.Opacity = 0.1;
            //f.Load += new EventHandler(f_Load);
            f.Load += new EventHandler(loadExMsgBoxForm);
            f.Show();
            return m_FormCloseButton;
        }
        
        public int ExEditValue(string Titre, string Message, out string EnteredValue)
        {
            m_Message = Message;
            m_Titre = Titre;
            m_MsgButtons = 1;

            Form f = new Form();
            f.Location = Screen.AllScreens[this.screenNumber].WorkingArea.Location;
            f.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            f.WindowState = FormWindowState.Maximized;
            f.TopMost = true;
            f.FormBorderStyle = FormBorderStyle.None;
            f.Opacity = 0.1;
            f.Load += new EventHandler(loadExEditValueForm);
            f.Show();
            EnteredValue = m_EnteredValue;
            return m_FormCloseButton;
        }

        private void loadExMsgBoxForm(object sender, EventArgs e)
        {
            MessageBoxButtons mb;
            // - OK (=0), OK + Annuler (=1), Oui + Non + Annuler (=3), Oui + Non (=4).
            switch (m_MsgButtons)
            {
                case 0:
                    mb = MessageBoxButtons.OK;
                    break;
                case 1:
                    mb = MessageBoxButtons.OKCancel;
                    break;
                case 3:
                    mb = MessageBoxButtons.YesNoCancel;
                    break;
                case 4:
                    mb = MessageBoxButtons.YesNo;
                    break;
                default:
                    mb = MessageBoxButtons.OK;
                    break;
            }

            ((Form)sender).Activate();

            ExMsgBoxForm exMsgBoxForm = new ExMsgBoxForm(m_buttonsLabels, m_Titre, m_Message, mb);
            
            exMsgBoxForm.ShowDialog();
            exMsgBoxForm.Activate();
            
            // Determine the state of the DialogResult property for the form.
            m_FormCloseButton = (int)exMsgBoxForm.DialogResult;

            // Close the transparent form calling this method
            ((Form)sender).Close();
        }

        private void loadExEditValueForm(object sender, EventArgs e)
        {
            ((Form)sender).Activate();

            // Always "OK + Cancel" for the Edit Value Form
            ExEditValueForm exEditValueForm = new ExEditValueForm(m_buttonsLabels, m_Titre, m_Message);
            
            exEditValueForm.ShowDialog();
            exEditValueForm.Activate();
            
            // Determine the state of the DialogResult property for the form.
            m_FormCloseButton = (int)exEditValueForm.DialogResult;

            m_EnteredValue = exEditValueForm.enteredValue;

            // Close the transparent form calling this method
            ((Form)sender).Close();
        }
    }
}
