﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptExtension
{
    public partial class ExEditValueForm : Form
    {
        public String enteredValue { get; set; }

        public ExEditValueForm(ButtonsLabelsConfigurator buttonsLabels, String Titre, String Message)
        {
            InitializeComponent();
            this.Text = Titre;
            this.valueToEditLabel.Text = Message;
            this.TopMost = true;
            
            if (buttonsLabels != null)
            {
                this.okButton.Text = buttonsLabels.okButtonLabel;
                this.cancelButton.Text = buttonsLabels.cancelButtonLabel;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void valueToEditTextBox_TextChanged(object sender, EventArgs e)
        {
            //this.enteredValue = this.valueToEditTextBox.Text;
            this.enteredValue = ((TextBox)sender).Text;
        }
    }
}
